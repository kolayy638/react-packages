import { observable, action } from 'mobx'
import Swagger from 'swagger-client';

function getUrlTree({ apiUrl }) {
  const l = document.createElement('a');
  l.href = apiUrl;
  return {
    scheme: l.protocol.slice(0, -1),
    host: `${l.hostname}:${l.port}`,
    basePath: l.pathname,
  };
}
async function resolveSwagger({ apiUrl, swaggerUrl }) {
  const { spec } = await Swagger.resolve({
    url: `${swaggerUrl}/swagger`
  })
  // parse api url and override the host and scheme in the swagger client
  const { scheme, host, basePath } = getUrlTree({ apiUrl })
  spec.schemes = [scheme]
  spec.host = host
  spec.basePath = basePath
  return spec
}

class RctSwaggerClientStore {
  @observable client = undefined

  @action async buildClient({ apiUrl, swaggerUrl }) {
    if (!apiUrl) {
      apiUrl = 'http://localhost:10010/v1'
    }
    if (!swaggerUrl) {
      swaggerUrl = apiUrl
    }
    const spec = await resolveSwagger({ apiUrl, swaggerUrl })
    this.client = await new Swagger({ spec })
  }
  @action async buildClientWithToken({ token }) {
    this.client = await new Swagger({
      spec: this.client.spec,
      authorizations: {
        AccountSecurity: token
      },
    })
  }
  @action async buildClientWithAuthorizations({ authorizations }) {
    this.client = await new Swagger({
      spec: this.client.spec,
      authorizations,
    })
  }
}

export default new RctSwaggerClientStore()
