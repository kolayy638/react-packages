# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="1.51.2"></a>
## [1.51.2](https://gitlab.com/4geit/react-packages/compare/v1.51.1...v1.51.2) (2017-10-04)


### Bug Fixes

* **notification-menu-component:** fix binding issue ([57b8bab](https://gitlab.com/4geit/react-packages/commit/57b8bab))




<a name="1.51.1"></a>
## [1.51.1](https://gitlab.com/4geit/react-packages/compare/v1.51.0...v1.51.1) (2017-10-04)


### Bug Fixes

* **notification-menu-store:** add observables and action methods ([e7944f9](https://gitlab.com/4geit/react-packages/commit/e7944f9))
* **notification-menu-store:** move mock data to store, add store to storybook ([f6295b4](https://gitlab.com/4geit/react-packages/commit/f6295b4))




<a name="1.50.0"></a>
# [1.50.0](https://gitlab.com/4geit/react-packages/compare/v1.49.0...v1.50.0) (2017-10-04)


### Bug Fixes

* **notification-menu-component:** add prop to define operationId to call endpoint ([be80f7e](https://gitlab.com/4geit/react-packages/commit/be80f7e))




<a name="1.48.2"></a>
## [1.48.2](https://gitlab.com/4geit/react-packages/compare/v1.48.1...v1.48.2) (2017-10-03)


### Bug Fixes

* **notification-menu-component:** add notification button, menu component and content ([731bcc6](https://gitlab.com/4geit/react-packages/commit/731bcc6))
* **notification-menu-component:** minor ([66fed9e](https://gitlab.com/4geit/react-packages/commit/66fed9e))




<a name="1.46.0"></a>
# [1.46.0](https://gitlab.com/4geit/react-packages/compare/v1.45.0...v1.46.0) (2017-10-03)


### Features

* **notification-menu-component:** create notification menu componenet and added it to storybook ([73b44e2](https://gitlab.com/4geit/react-packages/commit/73b44e2))
