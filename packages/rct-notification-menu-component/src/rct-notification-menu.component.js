import React, { Component } from 'react'
// eslint-disable-next-line
import PropTypes from 'prop-types'
import { propTypes, defaultProps } from 'react-props-decorators'
// eslint-disable-next-line
import { observable, action, toJS } from 'mobx'
// eslint-disable-next-line
import { inject, observer } from 'mobx-react'
import { withStyles } from 'material-ui/styles'
import withWidth from 'material-ui/utils/withWidth'
// eslint-disable-next-line
import classNames from 'classnames'
import IconButton from 'material-ui/IconButton'
import Icon from 'material-ui/Icon'
import Menu, { MenuItem } from 'material-ui/Menu'
import Avatar from 'material-ui/Avatar'
import Typography from 'material-ui/Typography'
import Grid from 'material-ui/Grid'
import Divider from 'material-ui/Divider'
import Button from 'material-ui/Button'

import './rct-notification-menu.component.css'

@withStyles(theme => ({
  // root: {
  //   [theme.breakpoints.down('md')]: {
  //     width: '100%',
  //   },
  // },
  // TBD
  // }
}))
@withWidth()
@inject('notificationMenuStore')
@observer
@propTypes({
  classes: PropTypes.object.isRequired,
  width: PropTypes.string.isRequired,
  operationId: PropTypes.string,
  // TBD
})
@defaultProps({
  // TBD
})

export default class RctNotificationMenuComponent extends Component {
  async componentWillMount() {
    // const { notificationMenuStore, operationId } = this.props
    // await notificationMenuStore.fetchData({ operationId })
  }
  handleClick(event) {
    this.props.notificationMenuStore.setOpen(true)
    this.props.notificationMenuStore.setElement(event.currentTarget)
  }
  handleRequestClose() {
    this.props.notificationMenuStore.setOpen(false)
  }

  render() {
    const { notificationMenuStore } = this.props
    return (
      <div>
        <IconButton
          onClick={this.handleClick.bind(this)}
        >
          <Icon>notifications</Icon>
        </IconButton>
        <Menu
          id='long-menu'
          element={notificationMenuStore.element}
          open={notificationMenuStore.open} onRequestClose={this.handleRequestClose.bind(this)}
        >
          <MenuItem style={{ background: 'white'}}>
            <Grid container justify='space-between'>
              <Grid item>
                <Typography type='title'>
                  Notifications
                </Typography>
              </Grid>
              <Grid item>
                <Button color='primary'>
                  See all
                </Button>
              </Grid>
            </Grid>
          </MenuItem>
          <Divider />
          {notificationMenuStore.data.map(({ avatar, title, background, link, date, user }) => (
            <MenuItem
              key={date}
              onClick={this.handleRequestClose.bind(this)}
              style={{ background: background, height: 60 }}
            >
              <Grid container>
                <Grid item>
                  <Avatar>
                    <Icon>{ avatar }</Icon>
                  </Avatar>
                </Grid>
                <Grid item xs>
                  <Typography>
                    <a href='#' style={{textDecoration: 'none'}}>{ user }</a>
                    { title }
                  </Typography>
                  <Typography type='caption'>
                    { date }
                  </Typography>
                </Grid>
              </Grid>
            </MenuItem>
          ))}
          <MenuItem>
            <Grid container justify='center'>
              <Grid item>
                <Button color='primary'>Load more</Button>
              </Grid>
            </Grid>
          </MenuItem>
        </Menu>
      </div>
    )
  }
}
