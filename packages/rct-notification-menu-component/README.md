# @4geit/rct-notification-menu-component [![npm version](//badge.fury.io/js/@4geit%2Frct-notification-menu-component.svg)](//badge.fury.io/js/@4geit%2Frct-notification-menu-component)

---

notification menu component to display a notification button with item menu

## Demo

A live storybook is available to see how the component looks like @ http://react-packages.ws3.4ge.it

## Installation

1. A recommended way to install ***@4geit/rct-notification-menu-component*** is through [npm](//www.npmjs.com/search?q=@4geit/rct-notification-menu-component) package manager using the following command:

```bash
npm i @4geit/rct-notification-menu-component --save
```

Or use `yarn` using the following command:

```bash
yarn add @4geit/rct-notification-menu-component
```

2. Depending on where you want to use the component you will need to import the class `RctNotificationMenuComponent` to your project JS file as follows:

```js
import RctNotificationMenuComponent from '@4geit/rct-notification-menu-component'
```

For instance if you want to use this component in your `App.js` component, you can use the RctNotificationMenuComponent component in the JSX code as follows:

```js
import React from 'react'
// ...
import RctNotificationMenuComponent from '@4geit/rct-notification-menu-component'
// ...
const App = () => (
  <div className="App">
    <RctNotificationMenuComponent/>
  </div>
)
```
