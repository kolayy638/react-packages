// eslint-disable-next-line
import { observable, action, runInAction, toJS } from 'mobx'
// eslint-disable-next-line
import { hashHistory } from 'react-router'

import swaggerClientStore from '@4geit/rct-swagger-client-store'
import notificationStore from '@4geit/rct-notification-store'

class RctNotificationMenuStore {
  @observable data = [
    {
      avatar: "date_range",
      title: " booked Test Activity for October 4, 2017 at 1pm. Credit Cardn Online",
      background: "#e6e6e6",
      link: "#",
      date: "08/31/2017 at 8:55pm",
      user: "Blake Pridham"
    },
    {
      avatar: "date_range",
      title: " booked Test Activity for October 10, 2017 at 5pm. Office POS",
      background: "#e6e6e6",
      link: "#",
      date: "08/31/2017 at 8:54pm",
      user: "Kevin Adams"
    },
    {
      avatar: "person_pin",
      title: ", guide, was assigned to Test Activity on October 2, 2017 at 1pm",
      background: "white",
      link: "#",
      date: "08/31/2017 at 8:53pm",
      user: "Ryan Stobie"
    },
    {
      avatar: "person_pin",
      title: ", guide, was assigned to Test Activity on October 5, 2017 at 1pm",
      background: "white",
      link: "#",
      date: "08/31/2017 at 8:52pm",
      user: "Phil Burgess"
    },
    {
      avatar: "date_range",
      title: " booked Test Activity for October 1, 2017 at 4pm. Credit Cardn Online",
      background: "#e6e6e6",
      link: "#",
      date: "08/31/2017 at 8:51pm",
      user: "Blake Pridham"
    },
  ]
  @observable open = false
  @observable element = undefined

  @action setOpen(value) {
    this.open = value
  }
  @action setElement(value) {
    this.element = value
  }
  @action appendData(value) {
    this.data = this.data || []
    this.data = this.data.concat(value)
    this.page++;
    console.log(this.data)
  }
  @action async fetchData({ operationId }) {
    try {
      console.log('fetchData')
      const { body: { list } } = await swaggerClientStore.client.apis.Account[operationId]()
      if (list && list.length) {
        // list.map( notification => {
        //   const { client, activity, payment, timestamp, refund, color, event, guide } = notification
        // })
        runInAction(() => {
          this.appendData(list)
        })
      }
    } catch (err) {
      console.error(err)
      runInAction(() => {
        notificationStore.newMessage(err.message)
      })
    }
  }
}

export default new RctNotificationMenuStore()
