# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="1.51.1"></a>
## [1.51.1](https://gitlab.com/4geit/react-packages/compare/v1.51.0...v1.51.1) (2017-10-04)


### Bug Fixes

* **notification-menu-store:** add observables and action methods ([e7944f9](https://gitlab.com/4geit/react-packages/commit/e7944f9))
* **notification-menu-store:** move mock data to store, add store to storybook ([f6295b4](https://gitlab.com/4geit/react-packages/commit/f6295b4))




<a name="1.51.0"></a>
# [1.51.0](https://gitlab.com/4geit/react-packages/compare/v1.50.0...v1.51.0) (2017-10-04)


### Features

* **notification-menu-store:** create notification menu store ([7d53464](https://gitlab.com/4geit/react-packages/commit/7d53464))
