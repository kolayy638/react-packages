import { observable, computed, action, runInAction, toJS } from 'mobx'
import { hashHistory } from 'react-router'

import swaggerClientStore from '@4geit/rct-swagger-client-store'
import notificationStore from '@4geit/rct-notification-store'

class RctChatboxGridStore {
  @observable data = []
  @computed get sortedData() {
    return this.data.slice().sort((a, b) => a.position > b.position)
  }
  @observable inProgress = false

  @action setData(value) {
    this.data = value
  }
  @action async fetchData({ listOperationId }) {
    listOperationId = listOperationId || 'userChatboxList'
    this.inProgress = true
    try {
      const { body } = await swaggerClientStore.client.apis.Account[listOperationId]()
      if (body.length) {
        runInAction(() => {
          this.setData( body )
          this.inProgress = false
        })
      }
    } catch (err) {
      console.error(err)
      runInAction(() => {
        notificationStore.newMessage(err.message)
      })
    }
  }
  @action async setPosition({ updateOperationId, itemId, position }) {
    updateOperationId = updateOperationId || 'userChatboxUpdate'
    this.inProgress = true
    try {
      const { body } = await swaggerClientStore.client.apis.Account[updateOperationId]()
      if (body.length) {
        runInAction(() => {
          this.setData( body )
          this.inProgress = false
        })
      }
    } catch (err) {
      console.error(err)
      runInAction(() => {
        notificationStore.newMessage(err.message)
      })
    }
  }
  @action async addItem({ addOperationId, listOperationId }) {
    addOperationId = addOperationId || 'userChatboxAdd'
    this.inProgress = true
    try {
      const { body } = await swaggerClientStore.client.apis.Account[addOperationId]()
      await this.fetchData({ listOperationId })
      runInAction(() => {
        this.inProgress = false
      })
    } catch (err) {
      console.error(err)
      runInAction(() => {
        notificationStore.newMessage(err.message)
      })
    }
  }
  @action async deleteItem ({ deleteOperationId, listOperationId }) {
    deleteOperationId = deleteOperationId || 'userChatboxDelete'
    this.inProgress = true
    try {
      const {body } = await
      swaggerClientStore.client.apis.Account[deleteOperationId]()
      await this.fetchData({ listOperationId })
      runInAction(() => {
        this.inProgress = false
      })
    } catch (err) {
      console.error(err)
      runInAction(() => {
        notificationStore.newMessage(err.message)
      })
    }
  }
  @action async activateItem({ updateOperationId, listOperationId }) {
    updateOperationId = updateOperationId || 'userChatboxUpdate'
    this.inProgress = true
    try {
      const { body } = await
      swaggerClientStore.client.apis.Account[updateOperationId]()
      await this.fetchData({ listOperationId })
      runInAction(() => {
        this.inProgress = false
      })
    } catch (err) {
      runInAction(() => {
        notificationStore.newMessage(err.message)
      })
    }
  }
  @action async unactivateItem({ updateOperationId, listOperationId }) {
    updateOperationId = updateOperationId || 'userChatboxUpdate'
    this.inProgress = true
    try {
      const { body } = await
      swaggerClientStore.client.apis.Account[updateOperationId]()
      await this.fetchData({ listOperationId })
      runInAction(() => {
        this.inProgress = false
      })
    } catch (err) {
      runInAction(() => {
        notificationStore.newMessage(err.message)
      })
    }
  }
  @action toggleMaximize(value) {
    this.data = value
  }
}

export default new RctChatboxGridStore()
