import { observable, action, runInAction, toJS } from 'mobx'
import { hashHistory } from 'react-router'
import moment from 'moment'

class RctBroadcastMessageStore {
  @observable message = ''
  @action setMessage(value) {
    this.message = value
  }
  @action reset() {
    this.message = ''
  }
  @action async send({ addOperationId }) {
    this.inProgress = true
    try {
      const { client: { apis: Account } } = swaggerClientStore
      const { body } = await Account[addOperationId]({
        body: {
          message: this.message,
          author: "Dummy Name",
          date: moment()
        }
      })
    } catch (err) {
      console.error(err)
      runInAction(() => {
        notificationStore.newMessage(err.message)
      })
    }
  }
}

export default new RctBroadcastMessageStore()
