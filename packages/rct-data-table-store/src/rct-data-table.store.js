import { observable, computed, action, runInAction, toJS } from 'mobx'
import { hashHistory } from 'react-router'

import swaggerClientStore from '@4geit/rct-swagger-client-store'
import notificationStore from '@4geit/rct-notification-store'

export class Column {
  @observable name
  @observable enabled = false

  constructor(name, enabled=false) {
    this.name = name
    this.enabled = enabled
  }

  @action setEnabled(value) {
    this.enabled = value
  }
}

class RctDataTableStore {
  @observable inProgress = false
  @observable open = false
  @observable element = undefined
  @observable columns = []
  @observable data = []

  @action setOpen(value) {
    this.open = value
  }
  @action setElement(value) {
    this.element = value
  }
  @action reset() {
    this.open = false
    this.element = undefined
  }
  @computed get enabledColumns() {
    return this.columns.filter(i => i.enabled)
  }
  @action setColumns(value) {
    this.columns = value.map(item => new Column(item.name, item.enabled))
  }
  @action setData(value) {
    this.data = value
  }
  @action async fetchData(operationId, enabledColumns) {
    this.inProgress = true
    try {
      const { body } = await swaggerClientStore.client.apis.Account[operationId]()
      if (body.length) {
        runInAction(() => {
          this.setColumns(
            Object.keys( body[0] ).map(i => ( { name: i, enabled: !!~enabledColumns.indexOf(i) } ))
          )
          this.setData( body )
          this.inProgress = false
        })
      }
    } catch (err) {
      console.error(err)
      runInAction(() => {
        notificationStore.newMessage(err.message)
      })
    }
  }
  @action addItem(value) {
    this.data = value
  }
  @action editItem(value) {
    this.data = value
  }
  @action removeItem(value) {
      this.data = value
  }
}

export default new RctDataTableStore()
