# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="1.50.0"></a>
# [1.50.0](https://gitlab.com/4geit/react-packages/compare/v1.49.0...v1.50.0) (2017-10-04)


### Bug Fixes

* **data-table-store:** fix brackets issue ([d092957](https://gitlab.com/4geit/react-packages/commit/d092957))


### Features

* **DataTable:** Add Remove button to data table ([472be8d](https://gitlab.com/4geit/react-packages/commit/472be8d))




<a name="1.49.0"></a>
# [1.49.0](https://gitlab.com/4geit/react-packages/compare/v1.48.2...v1.49.0) (2017-10-03)


### Bug Fixes

* **DataTable:** Edit button mior fix ([8a56f28](https://gitlab.com/4geit/react-packages/commit/8a56f28))


### Features

* **DataTable:** Add Edit button and click event ([1956374](https://gitlab.com/4geit/react-packages/commit/1956374))




<a name="1.48.0"></a>
# [1.48.0](https://gitlab.com/4geit/react-packages/compare/v1.47.1...v1.48.0) (2017-10-03)


### Bug Fixes

* **Datatable:** add icon ([2b2f16b](https://gitlab.com/4geit/react-packages/commit/2b2f16b))


### Features

* **DataTable:** add buton ([6c73e29](https://gitlab.com/4geit/react-packages/commit/6c73e29))




<a name="1.35.0"></a>
# [1.35.0](https://gitlab.com/4geit/react-packages/compare/v1.34.4...v1.35.0) (2017-09-20)




**Note:** Version bump only for package @4geit/rct-data-table-store

<a name="1.34.2"></a>
## [1.34.2](https://gitlab.com/4geit/react-packages/compare/v1.34.1...v1.34.2) (2017-09-20)




**Note:** Version bump only for package @4geit/rct-data-table-store

<a name="1.34.0"></a>
# [1.34.0](https://gitlab.com/4geit/react-packages/compare/v1.33.0...v1.34.0) (2017-09-20)




**Note:** Version bump only for package @4geit/rct-data-table-store

<a name="1.32.2"></a>
## [1.32.2](https://gitlab.com/4geit/react-packages/compare/v1.32.1...v1.32.2) (2017-09-18)




**Note:** Version bump only for package @4geit/rct-data-table-store

<a name="1.26.0"></a>
# [1.26.0](https://gitlab.com/4geit/react-packages/compare/v1.24.1...v1.26.0) (2017-09-11)


### Features

* **chatbox-list:** add new component and store ([833bed3](https://gitlab.com/4geit/react-packages/commit/833bed3))




<a name="1.25.0"></a>
# [1.25.0](https://gitlab.com/4geit/react-packages/compare/v1.24.1...v1.25.0) (2017-09-11)


### Features

* **chatbox-list:** add new component and store ([833bed3](https://gitlab.com/4geit/react-packages/commit/833bed3))




<a name="1.21.0"></a>
# [1.21.0](https://gitlab.com/4geit/react-packages/compare/v1.20.0...v1.21.0) (2017-09-08)


### Features

* **datatable:** add operationId and enabledColumns props and fetchData method ([760bf97](https://gitlab.com/4geit/react-packages/commit/760bf97))




<a name="1.8.0"></a>
# [1.8.0](https://gitlab.com/4geit/react-packages/compare/v1.7.2...v1.8.0) (2017-08-30)


### Features

* **layout:** add layout component jsx code ([904c8c6](https://gitlab.com/4geit/react-packages/commit/904c8c6))




<a name="1.7.2"></a>
## [1.7.2](https://gitlab.com/4geit/react-packages/compare/v1.7.1...v1.7.2) (2017-08-30)




**Note:** Version bump only for package @4geit/rct-data-table-store

<a name="1.7.0"></a>
# [1.7.0](https://gitlab.com/4geit/react-packages/compare/v1.6.15...v1.7.0) (2017-08-29)




**Note:** Version bump only for package @4geit/rct-data-table-store

<a name="1.6.14"></a>
## [1.6.14](https://gitlab.com/4geit/react-packages/compare/v1.6.13...v1.6.14) (2017-08-28)




**Note:** Version bump only for package @4geit/rct-data-table-store

<a name="1.6.11"></a>
## [1.6.11](https://gitlab.com/4geit/react-packages/compare/v1.6.10...v1.6.11) (2017-08-28)




**Note:** Version bump only for package @4geit/rct-data-table-store

<a name="1.6.10"></a>
## [1.6.10](https://gitlab.com/4geit/react-packages/compare/v1.6.9...v1.6.10) (2017-08-27)




**Note:** Version bump only for package @4geit/rct-data-table-store

<a name="1.6.9"></a>
## [1.6.9](https://gitlab.com/4geit/react-packages/compare/v1.6.8...v1.6.9) (2017-08-26)




**Note:** Version bump only for package @4geit/rct-data-table-store

<a name="1.6.8"></a>
## [1.6.8](https://gitlab.com/4geit/react-packages/compare/v1.6.7...v1.6.8) (2017-08-26)




**Note:** Version bump only for package @4geit/rct-data-table-store

<a name="1.6.7"></a>
## [1.6.7](https://gitlab.com/4geit/react-packages/compare/v1.6.6...v1.6.7) (2017-08-26)


### Bug Fixes

* **template:** fix minor issue ([a290cb9](https://gitlab.com/4geit/react-packages/commit/a290cb9))




<a name="1.6.6"></a>
## [1.6.6](https://gitlab.com/4geit/react-packages/compare/v1.6.5...v1.6.6) (2017-08-26)




**Note:** Version bump only for package @4geit/rct-data-table-store

<a name="1.6.5"></a>
## [1.6.5](https://gitlab.com/4geit/react-packages/compare/v1.6.4...v1.6.5) (2017-08-26)


### Bug Fixes

* **data-table:** replace createColumn by global setColumns method ([6372c41](https://gitlab.com/4geit/react-packages/commit/6372c41))




<a name="1.6.4"></a>
## [1.6.4](https://gitlab.com/4geit/react-packages/compare/1.6.3...1.6.4) (2017-08-26)




**Note:** Version bump only for package @4geit/rct-data-table-store
