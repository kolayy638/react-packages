import React, { Component } from 'react'
// eslint-disable-next-line
import PropTypes from 'prop-types'
import { propTypes, defaultProps } from 'react-props-decorators'
// eslint-disable-next-line
import { observable, action, toJS } from 'mobx'
// eslint-disable-next-line
import { inject, observer } from 'mobx-react'
import { withStyles } from 'material-ui/styles'
import withWidth from 'material-ui/utils/withWidth'
import Table, { TableBody, TableCell, TableHead, TableRow, TableSortLabel } from 'material-ui/Table'
import Menu, { MenuItem } from 'material-ui/Menu'
import Switch from 'material-ui/Switch'
import Paper from 'material-ui/Paper'
import Typography from 'material-ui/Typography'
import Checkbox from 'material-ui/Checkbox'
import Grid from 'material-ui/Grid'
import Toolbar from 'material-ui/Toolbar'
import IconButton from 'material-ui/IconButton'
import DeleteIcon from 'material-ui-icons/Delete';
import AddIcon from 'material-ui-icons/Add';
import EditIcon from 'material-ui-icons/Edit';
import ImportExportIcon from 'material-ui-icons/ImportExport'
import ViewColumnIcon from 'material-ui-icons/ViewColumn'
import FilterListIcon from 'material-ui-icons/FilterList'

import './rct-data-table.component.css'

@withStyles(theme => ({
  menuRoot: {
    overflowX: 'hidden',
  },
}))
@withWidth()
@inject('dataTableStore')
@observer
@propTypes({
  classes: PropTypes.object.isRequired,
  width: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  operationId: PropTypes.string.isRequired,
  enabledColumns: PropTypes.array,
  // TBD
})
@defaultProps({
  title: 'Data Table',
  enabledColumns: [],
  // TBD
})
export default class RctDataTableComponent extends Component {
  async componentWillMount() {
    const { operationId, enabledColumns } = this.props
    await this.props.dataTableStore.fetchData(operationId, enabledColumns)
  }
  componentWillUnmount() {
    this.props.dataTableStore.reset()
  }
  handleAddClick() {
    this.props.dataTableStore.addItem(true)
  }
  handleEditClick() {
    this.props.dataTableStore.editItem(true)
  }
  handleRemoveClick() {
    this.props.dataTableStore.removeItem(true)
  }
  handleClick(event) {
    this.props.dataTableStore.setOpen(true)
    this.props.dataTableStore.setElement(event.currentTarget)
  }
  handleRequestClose() {
    this.props.dataTableStore.setOpen(false)
  }
  handleChange(item) {
    return (event, checked) => item.setEnabled(checked)
  }

  render() {
    const { open, element, columns, enabledColumns, data } = this.props.dataTableStore
    const { title, classes } = this.props

    console.log(columns)

    if (!this.props.dataTableStore.data.length) {
      return (
        <Typography>Loading datatable...</Typography>
      )
    }
    return (
      <div>
        <Paper>
          <Toolbar>
            <Typography type="title" color="inherit" style={{
              flex: 1
            }}>
              { title }
            </Typography>
            <IconButton onClick={ this.handleAddClick.bind(this) }>
              <AddIcon/>
            </IconButton>
            <IconButton>
              <ImportExportIcon/>
            </IconButton>
            <IconButton
              onClick={ this.handleClick.bind(this) }
            >
              <ViewColumnIcon/>
            </IconButton>
            <IconButton>
              <DeleteIcon/>
            </IconButton>
            <Menu
              id="columns-menu"
              anchorEl={ element }
              open={ open }
              onRequestClose={ this.handleRequestClose.bind(this) }
              classes={ { root: classes.menuRoot } }
            >
              { columns.map((item) => (
                <MenuItem key={ item.name } >
                  <Grid container align="center">
                    <Grid item xs>
                      <Typography>{ item.name }</Typography>
                    </Grid>
                    <Grid item>
                      <Switch
                        checked={ item.enabled }
                        onChange={ this.handleChange(item) }
                      />
                    </Grid>
                  </Grid>
                </MenuItem>
              )) }
            </Menu>
          </Toolbar>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell checkbox>
                  <Checkbox checked/>
                </TableCell>
              { enabledColumns.map((item, index) => (
                <TableCell key={ index }>{item.name}</TableCell>
              )) }
                <TableCell>
                  <IconButton onClick={ this.handleEditClick.bind(this) }>
                    <EditIcon/>
                  </IconButton>
                  <IconButton onClick={ this.handleRemoveClick.bind(this) }>
                    <DeleteIcon/>
                  </IconButton>
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
            { data.map(item => (
              <TableRow hover key={ item.id } >
                <TableCell checkbox>
                  <Checkbox checked/>
                </TableCell>
              { enabledColumns.map((column, colIndex) => (
                <TableCell key={ colIndex } >{ item[column.name]}</TableCell>
              )) }
                <TableCell>
                  <IconButton onClick={ this.handleEditClick.bind(this) }>
                    <EditIcon/>
                  </IconButton>
                  <IconButton onClick={ this.handleRemoveClick.bind(this) }>
                    <DeleteIcon/>
                  </IconButton>
                </TableCell>
              </TableRow>
              )) }
            </TableBody>
          </Table>
        </Paper>
      </div>
    )
  }
}
