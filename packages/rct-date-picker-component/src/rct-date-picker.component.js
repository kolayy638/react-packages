import React, { Component } from 'react'
// eslint-disable-next-line
import PropTypes from 'prop-types'
import { propTypes, defaultProps } from 'react-props-decorators'
// eslint-disable-next-line
import { observable, action, toJS } from 'mobx'
// eslint-disable-next-line
import { inject, observer } from 'mobx-react'
import { withStyles } from 'material-ui/styles'
import withWidth from 'material-ui/utils/withWidth'
import compose from 'recompose/compose'
import classNames from 'classnames'
import TextField from 'material-ui/TextField'
import Button from 'material-ui/Button'
import Grid from 'material-ui/Grid'
import { SingleDatePicker } from 'react-dates'

import './rct-date-picker.component.css'
import 'react-dates/lib/css/_datepicker.css'

@withStyles(theme => ({
  root: {
    height: 50,
  }
}))
@withWidth()
@inject('datePickerStore')
@observer
@propTypes({
  classes: PropTypes.object.isRequired,
  width: PropTypes.string.isRequired,
  months: PropTypes.number,
  placeholder: PropTypes.string,
  calendarIcon: PropTypes.bool,
  // TBD
})
@defaultProps({
  // TBD
})
export default class RctDatePickerComponent extends Component {
  handleDate(date) {
    this.props.datePickerStore.setDate(date)
  }
  handleFocused({ focused }) {
    this.props.datePickerStore.setFocused(focused)
  }
  render() {
    const { classes, months, placeholder, calendarIcon, datePickerStore: { date, focused } } = this.props
    return (
      <SingleDatePicker
        placeholder={ placeholder }
        showDefaultInputIcon={ calendarIcon }
        date={ date }
        onDateChange={ this.handleDate.bind(this) }
        focused={ focused }
        onFocusChange={ this.handleFocused.bind(this) }
        numberOfMonths={ months }
      />
    )
  }
}
