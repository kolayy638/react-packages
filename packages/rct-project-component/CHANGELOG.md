# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="1.46.0"></a>
# [1.46.0](https://gitlab.com/4geit/react-packages/compare/v1.45.0...v1.46.0) (2017-10-03)




**Note:** Version bump only for package @4geit/rct-project-component

<a name="1.32.2"></a>
## [1.32.2](https://gitlab.com/4geit/react-packages/compare/v1.32.1...v1.32.2) (2017-09-18)




**Note:** Version bump only for package @4geit/rct-project-component

<a name="1.15.4"></a>
## [1.15.4](https://gitlab.com/4geit/react-packages/compare/v1.15.2...v1.15.4) (2017-09-06)


### Bug Fixes

* **project:** remove deprecated prop, add header component dep ([1d07e3e](https://gitlab.com/4geit/react-packages/commit/1d07e3e))




<a name="1.15.3"></a>
## [1.15.3](https://gitlab.com/4geit/react-packages/compare/v1.15.2...v1.15.3) (2017-09-05)


### Bug Fixes

* **project:** remove deprecated prop, add header component dep ([1d07e3e](https://gitlab.com/4geit/react-packages/commit/1d07e3e))




<a name="1.15.2"></a>
## [1.15.2](https://gitlab.com/4geit/react-packages/compare/v1.15.0...v1.15.2) (2017-09-05)


### Bug Fixes

* **project:** removed overlapping components from project ([806ca9b](https://gitlab.com/4geit/react-packages/commit/806ca9b))




<a name="1.15.1"></a>
## [1.15.1](https://gitlab.com/4geit/react-packages/compare/v1.15.0...v1.15.1) (2017-09-05)


### Bug Fixes

* **project:** removed overlapping components from project ([806ca9b](https://gitlab.com/4geit/react-packages/commit/806ca9b))




<a name="1.15.0"></a>
# [1.15.0](https://gitlab.com/4geit/react-packages/compare/v1.14.0...v1.15.0) (2017-09-04)


### Features

* **project:** improve project component ([ff95e5e](https://gitlab.com/4geit/react-packages/commit/ff95e5e))




<a name="1.14.0"></a>
# [1.14.0](https://gitlab.com/4geit/react-packages/compare/v1.12.0...v1.14.0) (2017-09-04)


### Bug Fixes

* **lerna:** fix versioning issue ([eee69c7](https://gitlab.com/4geit/react-packages/commit/eee69c7))


### Features

* **project component:** add main project component ([c9a4c36](https://gitlab.com/4geit/react-packages/commit/c9a4c36))




<a name="0.1.0"></a>
# [0.1.0](https://gitlab.com/4geit/react-packages/compare/v1.12.0...v0.1.0) (2017-09-04)


### Features

* **project component:** add main project component ([c9a4c36](https://gitlab.com/4geit/react-packages/commit/c9a4c36))
