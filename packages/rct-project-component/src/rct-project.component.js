import React, { Component } from 'react'
import { BrowserRouter, Switch, Route, withRouter, Link } from 'react-router-dom'
// eslint-disable-next-line
import PropTypes from 'prop-types'
import { propTypes, defaultProps } from 'react-props-decorators'
// eslint-disable-next-line
import { observable, action, toJS } from 'mobx'
// eslint-disable-next-line
import { Provider, inject, observer } from 'mobx-react'
import { withStyles } from 'material-ui/styles'
import withWidth from 'material-ui/utils/withWidth'
import compose from 'recompose/compose'
import classNames from 'classnames'

import './rct-project.component.css'

@withStyles(theme => ({
  // root: {
  //   [theme.breakpoints.down('md')]: {
  //     width: '100%',
  //   },
  // },
  // TBD
}))
@withWidth()
// @inject('xyzStore')
@observer
@propTypes({
  classes: PropTypes.object.isRequired,
  width: PropTypes.string.isRequired,
  layoutComponent: PropTypes.object.isRequired,
})
@defaultProps({
  // TBD
})
export default class RctProjectComponent extends Component {
  render() {
    const { layoutComponent, children } = this.props
    // affect project component children to layout component
    const _layoutComponent = React.cloneElement(layoutComponent, {}, children)
    return (
      <div>
        { _layoutComponent }
      </div>
    )
  }
}
