import React, { Component } from 'react'
// eslint-disable-next-line
import PropTypes from 'prop-types'
import { propTypes, defaultProps } from 'react-props-decorators'
// eslint-disable-next-line
import { observable, action, toJS } from 'mobx'
// eslint-disable-next-line
import { inject, observer } from 'mobx-react'
import { withStyles } from 'material-ui/styles'
import withWidth from 'material-ui/utils/withWidth'
import compose from 'recompose/compose'
import classNames from 'classnames'
import BottomNavigation, { BottomNavigationButton } from 'material-ui/BottomNavigation'
import Icon from 'material-ui/Icon'

import './rct-footer.component.css'

export const FooterItem = ({ icon, label }) => (
 <BottomNavigationButton label={ label } value={ label } icon={ <Icon>{ icon }</Icon> } />
)

@withStyles(theme => ({
  root: {
    width: 500,
  },
}))
@withWidth()
// @inject('xyzStore')
@observer
@propTypes({
  classes: PropTypes.object.isRequired,
})
@defaultProps({
  // TBD
})
export default class RctFooterComponent extends Component {
  state = {
    value: 'Recents',
  }

  handleChange = (event, value) => {
    this.setState({ value })
  }

  render() {
    const { classes, title, children } = this.props
    const { value } = this.state

    return (
      <BottomNavigation value={ value } onChange={ this.handleChange } className={ classes.root } >
        { children }
      </BottomNavigation>
    )
  }
}
