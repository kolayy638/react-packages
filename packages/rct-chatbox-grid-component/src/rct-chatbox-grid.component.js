import React, { Component } from 'react'
// eslint-disable-next-line
import PropTypes from 'prop-types'
import { propTypes, defaultProps } from 'react-props-decorators'
// eslint-disable-next-line
import { observable, action, toJS } from 'mobx'
// eslint-disable-next-line
import { inject, observer } from 'mobx-react'
import { withStyles } from 'material-ui/styles'
import withWidth from 'material-ui/utils/withWidth'
import compose from 'recompose/compose'
import classNames from 'classnames'
import { GridList, GridListTile, GridListTileBar } from 'material-ui/GridList';
import Subheader from 'material-ui/List/ListSubheader';
import Icon from 'material-ui/Icon';
import IconButton from 'material-ui/IconButton';
import DeleteIcon from 'material-ui-icons/Delete';
import List, { ListItem, ListItemText } from 'material-ui/List';
import Switch from 'material-ui/Switch';
import FullscreenIcon from 'material-ui-icons/Fullscreen';
import Grid from 'material-ui/Grid';
import Typography from 'material-ui/Typography';
import './rct-chatbox-grid.component.css'
import Card, { CardActions, CardContent } from 'material-ui/Card';
import Avatar from 'material-ui/Avatar';

import RctReorderableGridListComponent, { ReorderableGridListTile } from '@4geit/rct-reorderable-grid-list-component'

@withStyles(theme => ({
}))
class ItemComponent extends Component {
  handleClick() {
    this.props.chatboxGridStore.toggleMaximize(true)
  }
  render() {
    return (
      <div>
        <Card>
          <CardContent>
            <List>
              <ListItem
              style= { {
                  border: '2px solid darkgrey',
                  borderRadius:'10px',
                  margin: '2px 20px 1px 2px',
                  textAlign: 'left',
                  padding: '2px',
                } }
                button dense={true}>
                <Avatar style={ {
                  backgroundColor:"darkgrey",
                  width:'30',
                  height:'30',
                  }}>A</Avatar>
                <ListItemText primary="Hello"/>
              </ListItem>
              <ListItem
              style= { {
                  border: '2px solid lightgrey',
                  borderRadius:'10px',
                  margin: '10px 2px 1px 20px',
                  textAlign: 'right',
                  padding: '2px',
                } }
                button dense={true}>
                <ListItemText primary="How are you"/>
                <Avatar style={ {
                  backgroundColor:"lightgrey",
                  width:'30',
                  height:'30',
                }}>B</Avatar>
              </ListItem>
            </List>
          </CardContent>
          <CardActions>
            <Grid container align="center" style={ {
              height: '40px',
              paddingLeft: '10px',
              background: '#83ceec',
            } } >
              <Grid item xs>
                <Typography align="left">Name</Typography>
              </Grid>
              <Grid item >
                <Switch aria-label="checkedA"/>
                <IconButton onClick={ this.handleClick.bind(this) }>
                  <FullscreenIcon/>
                </IconButton>
                <IconButton>
                  <DeleteIcon/>
                </IconButton>
              </Grid>
            </Grid>
          </CardActions>
        </Card>
      </div>
    )
  }
}

@withStyles(theme => ({
  // TBD
}))
@withWidth()
@inject('chatboxGridStore')
@observer
@propTypes({
  classes: PropTypes.object.isRequired,
  listOperationId: PropTypes.string.isRequired,
  addOperationId: PropTypes.string.isRequired,
  updateOperationId: PropTypes.string.isRequired,
  deleteOperationId: PropTypes.string.isRequired,
})
@defaultProps({
  listOperationId: 'userChatboxList',
  addOperationId: 'userChatboxAdd',
  updateOperationId: 'userChatboxUpdate',
  deleteOperationId: 'userChatboxDelete',
})
export default class RctChatboxGridComponent extends Component {
  async componentWillMount() {
    const { listOperationId } = this.props
    await this.props.chatboxGridStore.fetchData({ listOperationId })
  }

  render() {
    const { classes, chatboxGridStore } = this.props
    const { typeContainer } = classes
    const { data, fetchData, setPosition, sortedData } = chatboxGridStore
    console.log(data)
    return (
      <RctReorderableGridListComponent
        cellHeight={ 180 }
        className={ classes.gridList }
        handleSetPosition={ setPosition.bind(chatboxGridStore) }
        handleFetchData={ fetchData.bind(chatboxGridStore) }
        data={ sortedData }
        itemComponent={ <ItemComponent/> }
        style={ { }}
        cols={ 4 }
      />
    )
  }
}
