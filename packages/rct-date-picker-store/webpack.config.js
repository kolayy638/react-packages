var path = require('path');

module.exports = {
  entry: './src/index.js',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'index.js',
    libraryTarget: 'umd',
  },
  externals: [
    /^\@4geit\/[a-zA-Z\-0-9]+$/,
    /^autosuggest\-highlight\/[a-zA-Z\-0-9]+$/,
    /^babel\-runtime\/[a-zA-Z\-0-9]+$/,
    'classnames',
    'fbjs',
    'keycode',
    'material-ui',
    /^material\-ui\/[a-zA-Z\-0-9]+$/,
    /^material\-ui\/[a-zA-Z\-0-9]+\/[a-zA-Z\-0-9]+$/,
    'material-ui-icons',
    /^material-ui-icons\/[a-zA-Z\-0-9]+$/,
    'mobx',
    'mobx-react',
    'prop-types',
    'react',
    /^react\-[a-zA-Z\-0-9]+$/,
    'recompose',
    /^recompose\/[a-zA-Z\-0-9]$/,
    'swagger-client',
    'typeface-roboto',
  ],
  module: {
    rules: [
      // js and jsx
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['react-app'],
            plugins: ['transform-decorators-legacy'],
          }
        }
      },
      // css
      {
        test: /\.css$/,
        use: [
          'style-loader',
          'css-loader'
        ]
      }
    ]
  }
};
