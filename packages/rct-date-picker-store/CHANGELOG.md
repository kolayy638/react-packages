# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="1.39.2"></a>
## [1.39.2](https://gitlab.com/4geit/react-packages/compare/v1.39.1...v1.39.2) (2017-09-21)


### Bug Fixes

* **date-picker:** fix proper use of action method of the store ([d90f3d2](https://gitlab.com/4geit/react-packages/commit/d90f3d2))
* **date-picker-component:** fix issue with observable assignment ([5ddf8f4](https://gitlab.com/4geit/react-packages/commit/5ddf8f4))
