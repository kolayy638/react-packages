# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="1.46.0"></a>
# [1.46.0](https://gitlab.com/4geit/react-packages/compare/v1.45.0...v1.46.0) (2017-10-03)


### Features

* **chatbox-list:** add switch logic add remove user chatbox ([8934788](https://gitlab.com/4geit/react-packages/commit/8934788))




<a name="1.35.0"></a>
# [1.35.0](https://gitlab.com/4geit/react-packages/compare/v1.34.4...v1.35.0) (2017-09-20)




**Note:** Version bump only for package @4geit/rct-chatbox-list-component

<a name="1.34.2"></a>
## [1.34.2](https://gitlab.com/4geit/react-packages/compare/v1.34.1...v1.34.2) (2017-09-20)




**Note:** Version bump only for package @4geit/rct-chatbox-list-component

<a name="1.34.0"></a>
# [1.34.0](https://gitlab.com/4geit/react-packages/compare/v1.33.0...v1.34.0) (2017-09-20)




**Note:** Version bump only for package @4geit/rct-chatbox-list-component

<a name="1.32.2"></a>
## [1.32.2](https://gitlab.com/4geit/react-packages/compare/v1.32.1...v1.32.2) (2017-09-18)




**Note:** Version bump only for package @4geit/rct-chatbox-list-component

<a name="1.26.0"></a>
# [1.26.0](https://gitlab.com/4geit/react-packages/compare/v1.24.1...v1.26.0) (2017-09-11)


### Features

* **chatbox-list:** add new component and store ([833bed3](https://gitlab.com/4geit/react-packages/commit/833bed3))




<a name="1.25.0"></a>
# [1.25.0](https://gitlab.com/4geit/react-packages/compare/v1.24.1...v1.25.0) (2017-09-11)


### Features

* **chatbox-list:** add new component and store ([833bed3](https://gitlab.com/4geit/react-packages/commit/833bed3))
