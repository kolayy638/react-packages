# @4geit/rct-login-component [![npm version](//badge.fury.io/js/@4geit%2Frct-login-component.svg)](//badge.fury.io/js/@4geit%2Frct-login-component)

---

login form component for react apps

## Demo

A live storybook is available to see how the component looks like @ http://rct-login-component.ws3.4ge.it

## Installation

1. A recommended way to install ***@4geit/rct-login-component*** is through [npm](//www.npmjs.com/search?q=@4geit/rct-login-component) package manager using the following command:

```bash
npm i @4geit/rct-login-component --save
```

Or use `yarn` using the following command:

```bash
yarn add @4geit/rct-login-component
```

Make sure you also have those tools installed globally in your system since @4geit/rct-login-component requires them to work properly:

* create-react-app: `sudo npm i -g create-react-app`
* storybook: `sudo npm i -g @storybook/cli`

2. Depending on where you want to use the component you will need to import the class `RctLoginComponent` to your project JS file as follows:

```js
import RctLoginComponent from '@4geit/rct-login-component'
```

For instance if you want to use this component in your `App.js` component, you can use the RctLoginComponent component in the JSX code as follows:

```js
import React from 'react'
// ...
import RctLoginComponent from '@4geit/rct-login-component'
// ...
const App = () => (
  <div className="App">
    <RctLoginComponent/>
  </div>
)
```

## Usage

1. You can actually use the component `rct-login-component` alone by running the command:

```bash
yarn start
```

Make sure the dependencies are all installed by running the command `yarn`.

2. `rct-login-component` comes along with `storybook` already setup therefore you can use it by running the command:

```bash
yarn storybook
```

The storybook is also deployed online and available @ http://rct-login-component.ws3.4ge.it
